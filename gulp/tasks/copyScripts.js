'use strict';

var gulp   = require('gulp');
var config = require('../config');

gulp.task('copyScripts', function() {

  return gulp.src([config.sourceDir + 'js/vendor/**/*'])
    .pipe(gulp.dest(config.buildDir + 'js/'));

});
