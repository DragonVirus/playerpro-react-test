'use strict';

var gulp   = require('gulp');
var config = require('../config');

gulp.task('copySemanticStyles', function() {

  return gulp.src([config.semanticDir + 'dist/**/*.css'])
    .pipe(gulp.dest(config.buildDir + 'css/'));

});
