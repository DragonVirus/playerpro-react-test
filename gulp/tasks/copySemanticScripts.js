'use strict';

var gulp   = require('gulp');
var config = require('../config');

gulp.task('copySemanticScripts', function() {

  return gulp.src([config.semanticDir + 'dist/**/*.js'])
    .pipe(gulp.dest(config.buildDir + 'js/'));
    
});
