'use strict';

var gulp   = require('gulp');
var config = require('../config');

gulp.task('copyIcons', function() {

  // Copy icons from root directory to build/
  return gulp.src([config.sourceDir + 'favicon.png', config.sourceDir + 'favicon.ico'])
    .pipe(gulp.dest(config.buildDir));

});
