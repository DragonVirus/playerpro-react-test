'use strict';

var gulp        = require('gulp');
var jsonServer  = require('gulp-json-srv');
var routes      = require('../../data/routes.json');

var server = jsonServer.start({
    data: 'data/db.json',
    port: 4000,
    rewriteRules: routes,
    deferredStart: true
});

gulp.task('apiServerStart', function () {
    server.start();
});

gulp.task('apiWatch', function () {
    gulp.watch(['data/db.json'], function(){
      server.reload();
    });
});

gulp.task('testApi', ['apiServerStart', 'apiWatch']);