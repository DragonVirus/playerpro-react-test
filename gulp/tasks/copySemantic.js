'use strict';

var gulp   = require('gulp');
var config = require('../config');

gulp.task('copySemantic', function() {

  return gulp.src([config.semanticDir + 'dist/**/*'])
    .pipe(gulp.dest(config.buildDir + 'semantic/'));
    
});
