'use strict';

module.exports = {

  'serverport': 3000,

  'scripts': {
    'src': './app/js/**/*.js',
    'dest': './build/js/'
  },

  'images': {
    'src': './app/images/**/*.{jpeg,jpg,png}',
    'dest': './build/images/'
  },

  'styles': {
    'src': './app/scss/**/*.scss',
    'dest': './build/css/'
  },
  
  'semanticDir': './semantic/',
  
  'sourceDir': './app/',

  'buildDir': './build/'

};
