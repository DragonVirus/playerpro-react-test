'use strict';

import React from 'react/addons';

const Footer = React.createClass({

  render() {
    return (
      <footer id="footer">

        {/* No content in footer for now */}

      </footer>
    );
  }

});

export default Footer;