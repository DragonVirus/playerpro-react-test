'use strict';

import React  from 'react/addons';
import {Link} from 'react-router';

const Header = React.createClass({

  render() {
    return (
      <header id="header" className="ui top fixed borderless menu">
        <div className="below tablet item" id="toggleLeft"><i className="large sidebar icon"></i></div>
        <Link className="item" to="/">
          <img className="logo" src="/images/pp-icon-orng.png" />
        </Link>
        <div className="ui left aligned category discover horizontally fitted item">
          <div className="ui icon input">
            <input className="prompt" type="text" placeholder="Search teams, players..." />
            <i className="search link icon"></i>
          </div>
          <div className="results"></div>
        </div>
        <div className="right menu">
          <Link to="/" className="link item"><i className="large home icon"></i></Link>
          <a href="#" className="ui simple dropdown item">
            <img className="ui avatar image" src="/images/wireframe/avatar_a.jpg" /> Elliot Smith <i className="dropdown icon"></i>
            <div className="menu">
              <div className="item"><i className="info icon"></i> Get Started</div>
              <div className="divider"></div>
              <div className="header">Settings</div>
              <div className="item"><i className="globe icon"></i> Global</div>
              <div className="item"><i className="user icon"></i> User</div>
              <div className="divider"></div>
              <div className="item"><i className="sign out icon"></i> Logout</div>
            </div>
          </a>
        </div>
      </header>
    );
  }

});

$(document).ready(function() {  
  $('#toggleLeft').click(function() {
    $(this).toggleClass('active');
    $('#leftSidebar').sidebar('toggle');
  });
});

export default Header;