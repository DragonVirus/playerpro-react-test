'use strict';

import React              from 'react/addons';
import {IndexLink, Link}  from 'react-router';

const LeftSidebar = React.createClass({

  render() {
    return (
      <div id="leftSidebar" className="ui sidebar inverted borderless vertical menu">
        <div className="item">
          <img className="ui mini avatar image" src="/images/wireframe/avatar_a.jpg" /> Elliot Smith
        </div>
        <IndexLink className="item" activeClassName="active" to="/">
          <i className="announcement icon"></i> Feed
        </IndexLink>
        <Link className="item" activeClassName="active" to="/schedule">
          <i className="calendar outline icon"></i> Schedule
        </Link>
        <Link className="item" activeClassName="active" to="/photos">
          <i className="camera retro icon"></i> Photos
        </Link>
        <Link className="item" activeClassName="active" to="/discover">
          <i className="search icon"></i> Discover
        </Link>
        <div className="header item">Teams</div>
        <Link to="/team" className="flex item">
          <div className="ui micro image">
            <img src="/images/wireframe/crest_b.png" />
          </div>
          <div className="middle aligned content">
            PlayerPro Team
          </div>
        </Link>
        <Link to="/team" className="flex item">
          <div className="ui micro image">
            <img src="/images/wireframe/crest_a.png" />
          </div>
          <div className="middle aligned content">
            Vampiros MX
          </div>
        </Link>
        <div className="header item">Competitions</div>
        <a className="flex item">
          <div className="ui micro image">
            <img src="/images/wireframe/crest_c.png" />
          </div>
          <div className="middle aligned content">
            Fraternidad
          </div>
        </a>
        <a className="item">
          Most Popular
        </a>
      </div>
    );
  }

});

export default LeftSidebar;