'use strict';

import React      from 'react/addons';
import FeedCard   from './FeedCard';

const Feed = React.createClass({

  render() {
    return (
      <div id="feed">
        <FeedCard />
      </div>
    );
  }

});

export default Feed;