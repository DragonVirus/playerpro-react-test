'use strict';

import React from 'react';
import QuickRsvpCard from './QuickRsvpCard';
import EventService from '../../services/EventService';
import RsvpService from '../../services/RsvpService';
import _ from 'lodash';

const update = React.addons.update;

//how many events are we able to display at same time?
const MAX_EVENTS = 2;

//animations duration in millis
const TIMEOUT_ANIMATION = 1000;

const QuickRsvp = React.createClass({
	propTypes: {
		userId: React.PropTypes.number.isRequired,
	},

	getInitialState(){
		return {
			closest: [],

			queue: [],

			rsvpingToEvent: false,
		};
	},

	componentDidMount(){
		const onMissingRsvpEvents = (invites) => {
			console.log('invites:', invites);

			if (this.isMounted()){
				const closest = _.take(invites, MAX_EVENTS);
				const queue = _.drop(invites, MAX_EVENTS);

				console.log('closest:', closest);
				console.log('queue:', queue);

				this.setState({closest, queue});
			}
		};
		
		EventService.getMissingRsvpEvents().then(onMissingRsvpEvents);
	},

	rsvpToEvent(i, status){
		if (this.state.rsvpingToEvent){
			return;
		}

		this.setState({rsvpingToEvent: true});

		const invite = this.state.closest[i];
		
		console.log(invite, status);

		const onRsvpToEvent = () => {
			//triggers CSS animation
			this.setState(update(this.state, {
				closest: {
					[i]: {
						animation: {$set: QuickRsvpCard.ANIMATION_REMOVE}
					}
				}
			}));

			const onRemoveAnimation = () => {
				this.setState({rsvpingToEvent: false});

				if (_.isEmpty(this.state.queue)){
					this.setState(update(this.state, {
						closest: {$splice: [[i, 1]]},
					}));
				}
				else {
					const next = _.first(this.state.queue);

					this.setState(update(this.state, {
						queue: {$splice: [[0, 1]]},

						closest: {
							[i]: {$set: next}
						}
					}));

					//triggers CSS animation
					this.setState(update(this.state, {
						closest: {
							[i]: {
								animation: {$set: QuickRsvpCard.ANIMATION_ADD}
							}
						}
					}));

					const onAddAnimation = () => {
						//triggers CSS animation
						this.setState(update(this.state, {
							closest: {
								[i]: {
									animation: {$set: QuickRsvpCard.ANIMATION_NONE}
								}
							}
						}));
					};

					setTimeout(onAddAnimation, TIMEOUT_ANIMATION);
				}
			};

			setTimeout(onRemoveAnimation, TIMEOUT_ANIMATION);
		};

		RsvpService.rsvpToEvent().then(onRsvpToEvent);
	},

	render(){
		if (_.isEmpty(this.state.closest)){
			return false;
		}

		const cards = this.state.closest.map((invite, i) =>
			<QuickRsvpCard key={i}
				idx={i}
				invite={invite} 
				onRsvpToEvent={this.rsvpToEvent} />
		);

		return(
			<div className="module">
				<div className="ui small header">Pending Invites</div>
				<div className="ui cards">
					{cards}
				</div>
			</div>
		);
	},
});

export default QuickRsvp;
