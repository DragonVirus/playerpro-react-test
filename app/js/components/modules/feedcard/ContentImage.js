'use strict';

import React      from 'react/addons';

const ContentImage = React.createClass({
  
  render() {
    return (
      <div className="image">
        <img src="/images/wireframe/placehold_a.jpg" />
      </div>
    );
  }

});

export default ContentImage;