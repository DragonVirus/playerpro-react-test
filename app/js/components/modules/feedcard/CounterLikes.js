'use strict';

import React      from 'react/addons';

const Comment = React.createClass({
  
  componentDidMount() {
    $( ".ui.dropdown" ).click(function() {
      $( this ).dropdown();
    });
  },

  render() {
    return (
      <div className="ui basic compact icon button counter">
        <i className="large pp-heart icon"></i> 17
      </div>
    );
  }

});

export default Comment;