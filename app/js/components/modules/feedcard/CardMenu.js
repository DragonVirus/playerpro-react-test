'use strict';

import React      from 'react/addons';
import {Link}     from 'react-router';

const CardMenu = React.createClass({
  
  componentDidMount() {
    $( ".ui.dropdown" ).click(function() {
      $( this ).dropdown();
    });
  },

  render() {
    return (
      <div className="ui downward dropdown basic compact icon button right floated">
        <i className="large ellipsis vertical icon"></i>
        <div className="menu">
          <div className="item">View</div>
          <div className="item">Edit</div>
          <div className="divider"></div>
          <div className="item">Report</div>
          <div className="item">Delete</div>
        </div>
      </div>
    );
  }

});

export default CardMenu;