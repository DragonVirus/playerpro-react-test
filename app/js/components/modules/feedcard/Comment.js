'use strict';

import React      from 'react/addons';
import {Link}     from 'react-router';

const Comment = React.createClass({
  
  componentDidMount() {
    $( ".ui.dropdown" ).click(function() {
      $( this ).dropdown();
    });
  },

  render() {
    return (
      <div className="comment">
        <a className="avatar">
          <img src="/images/wireframe/avatar_c.jpg" />
        </a>
        <div className="content">
          <a className="author">Elliot Fu</a>
          <div className="metadata">
            <span className="date">Yesterday at 12:30AM</span>
          </div>
          <div className="text">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed, distinctio, quaerat, similique aliquam natus fugiat quae pariatur saepe quis dolores minus doloribus harum tenetur nam dolorem.</p>
          </div>
        </div>
      </div>
    );
  }

});

export default Comment;