'use strict';

import React      from 'react/addons';

const ContentText = React.createClass({
  
  render() {
    return (
      <div className="content">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, autem, ipsum modi saepe sed quo provident veniam eligendi doloribus odio quia et omnis voluptatem magni quae voluptatibus impedit. Hic, impedit!
      </div>
    );
  }

});

export default ContentText;