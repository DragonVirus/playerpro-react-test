'use strict';

import React      from 'react/addons';

const Comment = React.createClass({
  
  componentDidMount() {
    $( ".ui.dropdown" ).click(function() {
      $( this ).dropdown();
    });
  },

  render() {
    return (
      <div className="ui basic compact icon button counter">
        <i className="large pp-comment icon"></i> 3
      </div>
    );
  }

});

export default Comment;