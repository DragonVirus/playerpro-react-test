'use strict';

import React      from 'react/addons';

const ContentLink = React.createClass({
  
  render() {
    return (
      <div className="content link">
        <a className="ui segment">
          <img src="/images/wireframe/placehold_a.jpg" />
          <h4 className="header">
            Link Title Here
          </h4>
          <div className="description">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, alias, vitae, numquam laboriosam mollitia assumenda quos aut debitis est velit aspernatur obcaecati possimus libero minus quia aliquid minima harum at.
          </div>
          <div className="meta">
            website.com
          </div>
        </a>
      </div>
    );
  }

});

export default ContentLink;