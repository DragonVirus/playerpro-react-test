'use strict';

import React              from 'react/addons';
import {Link}             from 'react-router';

import CardMenu           from './feedcard/CardMenu';
import Comment            from './feedcard/Comment';
import CounterLikes       from './feedcard/CounterLikes';
import CounterComments    from './feedcard/CounterComments';

import ContentText        from './feedcard/ContentText';
import ContentImage       from './feedcard/ContentImage';
import ContentLink        from './feedcard/ContentLink';

const FeedCard = React.createClass({
  
  render() {
    return (
      <div className="ui fluid card">
        <div className="item">
          <div className="ui image">
            <img className="ui huge avatar image" src="/images/wireframe/avatar_a.jpg" />
          </div>
          <div className="middle aligned content">
            Elliot Smith
            <div className="meta">14 hours ago</div>
          </div>
          <div className="middle aligned meta">
            <i className="large pp-globe icon"></i>Public
          </div>
          <div className="middle aligned last">
            <CardMenu />
          </div>
        </div>
        
        {/* These blocks are conditional based on post content. Some posts may have content text accompanying an image or link. */}
        <ContentText />
        <ContentImage />
        <ContentLink />
        
        <div className="content">
          <CounterComments />
          <CounterLikes />
        </div>
        <div className="extra content">
          <div className="ui minimal comments">
            <Comment />
            <Comment />
            <Comment />
            <Comment />
          </div>
        </div>
        <div className="extra content">
          <div className="ui large transparent left icon input">
            <i className="large pp-heart icon"></i>
            <input type="text" placeholder="Add Comment..." />
          </div>
        </div>
      </div>
    );
  }

});

export default FeedCard;