'use strict';

import React from 'react';
import {Link} from 'react-router';
import {FormattedDate, FormattedTime} from 'react-intl';
import RsvpService from '../../services/RsvpService';

const QuickRsvpCard = React.createClass({
	statics: {
		ANIMATION_REMOVE: 'animated bounceOutRight',

		ANIMATION_ADD: 'animated fadeIn',

		ANIMATION_NONE: ''
	},

	propTypes: {
		idx: React.PropTypes.number.isRequired,

		invite: React.PropTypes.object.isRequired,
	},

	rsvpToEvent(status){
		this.props.onRsvpToEvent(this.props.idx, status);
	},

	render(){
		return(
			<div className={'card ' + this.props.invite.animation}>
				<Link to={'/event/' + this.props.invite.id} className="ui items">
					<div className="item">
						<div className="ui mini image">
							<img src={this.props.invite.homeTeam.crestPictureUrl} />
						</div>
						<div className="middle aligned content">
							{this.props.invite.homeTeam.name}
						</div>
					</div>
					<div className="item">
						<div className="ui mini image">
							<img src={this.props.invite.awayTeam.crestPictureUrl} />
						</div>
						<div className="middle aligned content">
							{this.props.invite.awayTeam.name}
						</div>
					</div>
				</Link>

				<div className="content">
					<div className="description">
					  <FormattedDate value={this.props.invite.date * 1000} day="numeric" month="short" year="numeric" /> @ <FormattedTime value={this.props.invite.time * 1000} hour="numeric" minute="2-digit" hour12 />
					</div>
					<div className="meta">
						{this.props.invite.venue.name}
					</div>
				</div>

				<div className="ui bottom attached three buttons">
					<div className="ui green vertical animated button"
						onClick={this.rsvpToEvent.bind(this, RsvpService.STATUS_YES)}>
						<div className="hidden content">Yes</div>
						<div className="visible content">
							<i className="large pp-yes icon"></i>
						</div>
					</div>
					<div className="ui yellow vertical animated button"
						onClick={this.rsvpToEvent.bind(this, RsvpService.STATUS_MAYBE)}>
						<div className="hidden content">Maybe</div>
						<div className="visible content">
							<i className="large pp-maybe icon"></i>
						</div>
					</div>
					<div className="ui red vertical animated button"
						onClick={this.rsvpToEvent.bind(this, RsvpService.STATUS_NO)}>
						<div className="hidden content">No</div>
						<div className="visible content">
							<i className="large pp-no icon"></i>
						</div>
					</div>
				</div>
			</div>
		);
	},
});

export default QuickRsvpCard;
