'use strict';

import React from 'react/addons';

const QuickRsvpCard = React.createClass({
	render(){
		return(
			<div className="card">
				<a href="#" className="ui items">
					<div className="item">
						<div className="ui mini image">
							<img src="/images/wireframe/crest_a.png" />
						</div>
						<div className="middle aligned content">
							Team Name A
						</div>
					</div>
					<div className="item">
						<div className="ui mini image">
							<img src="/images/wireframe/crest_b.png" />
						</div>
						<div className="middle aligned content">
							Team Name B
						</div>
					</div>
				</a>

				<div className="content">
					<div className="description">
						October 10, 2015 @ 5:00pm
					</div>
					<div className="meta">
						Crocker Amazon - Field A
					</div>
				</div>

				<div className="ui bottom attached three buttons">
					<div className="ui green vertical animated button">
						<div className="hidden content">Yes</div>
						<div className="visible content">
							<i className="large pp-yes icon"></i>
						</div>
					</div>
					<div className="ui yellow vertical animated button">
						<div className="hidden content">Maybe</div>
						<div className="visible content">
							<i className="large pp-maybe icon"></i>
						</div>
					</div>
					<div className="ui red vertical animated button">
						<div className="hidden content">No</div>
						<div className="visible content">
							<i className="large pp-no icon"></i>
						</div>
					</div>
				</div>
			</div>
		);
	},

	propTypes: {
		invite: React.PropTypes.object.isRequired
	}
});

export default QuickRsvpCard;
