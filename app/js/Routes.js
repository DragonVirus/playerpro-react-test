'use strict';

import React                        from 'react/addons';
import {Router, 
        Route, 
        IndexRoute, 
        browserHistory}  from 'react-router';

import App                          from './App';
import HomePage                     from './pages/HomePage';
import FeedPage                     from './pages/FeedPage';
import SchedulePage                 from './pages/SchedulePage';
import PhotosPage                   from './pages/PhotosPage';
import DiscoverPage                 from './pages/DiscoverPage';

import TeamPage                     from './pages/TeamPage';

import NotFoundPage                 from './pages/NotFoundPage';

export default (
  <Router history={browserHistory}>
    <Route path="/" component={App}>

      <IndexRoute component={FeedPage} />

      <Route path="/" component={HomePage} />
      <Route path="/feed" component={FeedPage} />
      <Route path="/schedule" component={SchedulePage} />
      <Route path="/discover" component={DiscoverPage} />
      <Route path="/photos" component={PhotosPage} />
      
      <Route path="/team" component={TeamPage} />

      <Route path="*" component={NotFoundPage} />

    </Route>
  </Router>
  
);