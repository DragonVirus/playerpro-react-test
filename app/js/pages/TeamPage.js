'use strict';

import React          from 'react/addons';
import {Link}         from 'react-router';
import ImageLoad      from '../components/ImageLoad';
import Document       from '../components/Document';
import MenuMain       from '../components/MenuMain';

const TeamPage = React.createClass({

  propTypes: {
    currentUser: React.PropTypes.object.isRequired
  },

  render() {
    return (
      <Document title="Team Name - PlayerPro">
        <div className="ui main grid container">
                
          <div className="three wide column above tablet">
          
            <MenuMain />
            
          </div>
          
          <div className="twelve wide computer sixteen wide tablet column">
          
            <div className="ui rounded billboard">
            
              <div className="ui title items">
                <div className="item">
                  <div className="ui tiny image">
                    <img className="ui tiny image" src="https://playerpro-upload.s3.amazonaws.com/2015/05/27/55660fb674207_thumbnail.png" />
                  </div>
                  <div className="middle aligned content">
                    <h2 className="ui inverted header">
                      Team Name
                      <a className="sub header">League Name</a>
                    </h2>
                  </div>
                  <div className="middle aligned content">
                  
                  </div>
                </div>
              </div>
              
              <img className="horizontal centered background image" src="/images/wireframe/cover_a.jpg" />
            </div>
            
          </div>
          
          <div className="one wide column">
            <button className="ui icon button">
              <i className="user icon"></i>
            </button>
          </div>
          
        </div>
      </Document>
    );
  }

});

export default TeamPage;