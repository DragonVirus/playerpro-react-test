'use strict';

import React          from 'react/addons';
import {Link}         from 'react-router';
import Document       from '../components/Document';

import MenuMain       from '../components/MenuMain';

const DiscoverPage = React.createClass({

  propTypes: {
    currentUser: React.PropTypes.object.isRequired
  },

  render() {
    return (
      <Document title="Discover - PlayerPro">
        <div className="ui main grid container">
                
          <div className="three wide column above tablet">
          
            <MenuMain />
            
          </div>
          
          <div id="discover" className="thirteen wide computer sixteen wide tablet column">
          
            <h1 className="ui header">Discover</h1>
          
            <div className="ui three doubling cards">
              <div className="card">
                <div className="image">
                  <img src="/images/wireframe/avatar_b.jpg" />
                </div>
                <div className="content">
                  <div className="header">Matt Giampietro</div>
                  <div className="meta">
                    <a>Friends</a>
                  </div>
                  <div className="description">
                    Matthew is an interior designer living in New York.
                  </div>
                </div>
                <div className="ui bottom attached button">
                  <i className="add icon"></i>
                  Follow
                </div>
              </div>
              <div className="card">
                <div className="image">
                  <img src="/images/wireframe/avatar_d.jpg" />
                </div>
                <div className="content">
                  <div className="header">Molly</div>
                  <div className="meta">
                    <span className="date">Coworker</span>
                  </div>
                  <div className="description">
                    Molly is a personal assistant living in Paris.
                  </div>
                </div>
                <div className="ui bottom attached button">
                  <i className="add icon"></i>
                  Follow
                </div>
              </div>
              <div className="card">
                <div className="image">
                  <img src="/images/wireframe/avatar_e.jpg" />
                </div>
                <div className="content">
                  <div className="header">Elyse</div>
                  <div className="meta">
                    <a>Coworker</a>
                  </div>
                  <div className="description">
                    Elyse is a copywriter working in New York.
                  </div>
                </div>
                <div className="ui bottom attached button">
                  <i className="add icon"></i>
                  Follow
                </div>
              </div>
              <div className="card">
                <div className="image">
                  <img src="/images/wireframe/avatar_c.jpg" />
                </div>
                <div className="content">
                  <div className="header">Molly</div>
                  <div className="meta">
                    <span className="date">Coworker</span>
                  </div>
                  <div className="description">
                    Molly is a personal assistant living in Paris.
                  </div>
                </div>
                <div className="ui bottom attached button">
                  <i className="add icon"></i>
                  Follow
                </div>
              </div>
              <div className="card">
                <div className="image">
                  <img src="/images/wireframe/avatar_a.jpg" />
                </div>
                <div className="content">
                  <div className="header">Molly</div>
                  <div className="meta">
                    <span className="date">Coworker</span>
                  </div>
                  <div className="description">
                    Molly is a personal assistant living in Paris.
                  </div>
                </div>
                <div className="ui bottom attached button">
                  <i className="add icon"></i>
                  Follow
                </div>
              </div>
            </div>              
          </div>
          
        </div>
      </Document>
    );
  }

});

export default DiscoverPage;