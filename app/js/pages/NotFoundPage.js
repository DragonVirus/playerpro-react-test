'use strict';

import React          from 'react/addons';
import Document       from '../components/Document';

const NotFoundPage = React.createClass({

  propTypes: {
    currentUser: React.PropTypes.object.isRequired
  },

  render() {
    return (
      <Document title="404: Not Found - PlayerPro">
        <section className="ui main text container">

          <h1 className="ui header">Not Found</h1>
          <p>This is not the file you are looking for...</p>

        </section>
      </Document>
    );
  }

});

export default NotFoundPage;