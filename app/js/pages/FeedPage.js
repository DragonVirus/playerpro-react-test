'use strict';

import React          from 'react/addons';
import {Link}         from 'react-router';
import Document       from '../components/Document';
import MenuMain       from '../components/MenuMain';
import QuickRsvp      from '../components/modules/QuickRsvp';
import Feed           from '../components/modules/Feed';

const FeedPage = React.createClass({

  propTypes: {
    currentUser: React.PropTypes.object.isRequired
  },
  
  render() {
    return (
      <Document title="Feed - PlayerPro">
        <div className="ui main grid container">
                  
          <div className="three wide column above tablet">
          
            <MenuMain />
            
          </div>
          
          <div id="feed" className="nine wide computer ten wide tablet sixteen wide mobile column">
          
            <Feed />
                      
            {/*<div className="ui fluid card">
              <div className="content">
                <div className="right floated meta">14h</div>
                <img className="ui avatar image" src="/images/wireframe/avatar_a.jpg" /> Elliot Smith
              </div>
              <div className="image">
                <img src="/images/wireframe/placehold_a.jpg" />
              </div>
              <div className="content">
                <span className="right floated">
                  <i className="heart outline like icon"></i>
                  17 likes
                </span>
                <i className="comment icon"></i>
                3 comments
              </div>
              <div className="extra content">
                <div className="ui large transparent left icon input">
                  <i className="heart outline icon"></i>
                  <input type="text" placeholder="Add Comment..." />
                </div>
              </div>
            </div>
            
            <div className="ui fluid card">
              <div className="content">
                <div className="right floated meta">14h</div>
                <img className="ui avatar image" src="/images/wireframe/avatar_a.jpg" /> Elliot Smith
              </div>
              <div className="image">
                <img src="/images/wireframe/placehold_a.jpg" />
              </div>
              <div className="content">
                <span className="right floated">
                  <i className="heart outline like icon"></i>
                  17 likes
                </span>
                <i className="comment icon"></i>
                3 comments
              </div>
              <div className="extra content">
                <div className="ui large transparent left icon input">
                  <i className="heart outline icon"></i>
                  <input type="text" placeholder="Add Comment..." />
                </div>
              </div>
            </div>*/}
            
          </div>
          
          <div className="computer only tablet only four wide computer six wide tablet column">
          
            <QuickRsvp />
                        
          </div>
                    
        </div>
      </Document>
    );
  }

});

export default FeedPage;