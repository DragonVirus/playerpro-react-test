'use strict';

import React          from 'react/addons';
import {Link}         from 'react-router';

import Document       from '../components/Document';
import MenuMain       from '../components/MenuMain';


const HomePage = React.createClass({

  propTypes: {
    currentUser: React.PropTypes.object.isRequired
  },

  render() {
    var loginButton = <button className="ui orange button" id="open_login">Login</button>;
    
    return (
      <Document title="PlayerPro - Home" bodyClass="home-page">
        <div className="ui main grid container">
                  
          <div className="ten wide column">
            <h1>Signup</h1>
          </div>
          
          <div className="six wide column">
            
            <h3 className="ui inverted header">Signup</h3>
            
            <div className="ui segment">
              <p><i className="pp-field icon"></i>Icon testing!</p>
              <p><i className="close icon"></i>Icon testing!</p>
            </div>
            
            <div className="ui icon buttons">
              <button className="ui button"><i className="pp-field icon"></i></button>
              <button className="ui button"><i className="pp-yes icon"></i></button>
              <button className="ui button"><i className="pp-maybe icon"></i></button>
              <button className="ui button"><i className="pp-no icon"></i></button>
            </div>
          
            <button className="ui orange button" id="open_login">Login</button>
                      
          </div>
          
          <div className="ui mini modal" id="modal_login">
            <div className="header">
              Login
            </div>
            <div className="content">
              <div className="ui form">
                <div className="field">
                  <label>Email</label>
                  <input type="email" placeholder="Email" />
                </div>
                <div className="field">
                  <label>Password</label>
                  <input type="password" placeholder="Password" />
                </div>
                <div className="field inline">
                  <div className="ui toggle checkbox">
                    <input type="checkbox" tabindex="0" className="hidden" />
                    <label>Remember Me</label>
                  </div>
                </div>
              </div>
            </div>
            <div className="actions">
              <div className="ui basic button left floated">Forgot Password</div>
              <div className="ui cancel button">Cancel</div>
              <Link className="ui green approve button" to="/feed">Submit</Link>
            </div>
          </div>
                    
        </div>
      </Document>
    );
  }

});

$(document).ready(function() {
  $('.ui.checkbox').checkbox();
   
  $('#open_login').click(function() {
    $('#modal_login').modal('show');
  });
});

export default HomePage;