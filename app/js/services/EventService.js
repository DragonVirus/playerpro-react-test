'use strict';

const EventService = {};

EventService.TYPE_GAME = 'Game';
EventService.TYPE_PRACTICE = 'Practice';
EventService.TYPE_MEETING = 'Meeting';
EventService.TYPE_PICKUP = 'Pickup';

//TODO: call ajax endpoint
EventService.getMissingRsvpEvents = function(){
	
	var eventsApi = $.getJSON("http://localhost:4000/v1/events");

	return new Promise((resolve, reject) => {
		resolve(eventsApi);
	});
};

export default EventService;
