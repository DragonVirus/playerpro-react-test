playerpro-react-test
====================

Greetings and welcome new PlayerPro web developer candidates!

## Technology Overview

#### Assumptions

This test makes some initial assumptions that you have existing experience with technologies such as [Git](https://git-scm.com/), [Node](https://nodejs.org), [npm](https://www.npmjs.com/), and [Gulp](http://gulpjs.com/). We also assume that you have experience working with JSON data returned by a RESTful API.

#### React

We use [React](https://facebook.github.io/react/) to build all of our user interface view components on PlayerPro. You will want to be familiar with how React components handle properties and state, and how to use component life cyle properties to work with data within these components.

#### Reflux

We use the unidirectional data architecture [Reflux](https://github.com/reflux/refluxjs) for our web application.

## The Test

#### Overview

We would like you to go through the process of creating working React components with Flux actions and stores with data provided by a mock API within this test project.

#### Steps

1. Create Flux actions and stores for post feed data
2. Connect stores to the mock API at [`http://localhost:4000/posts`](http://localhost:4000/posts)
3. Connect Flux stores to the React `Feed` component
4. Feed props to all subcomponents of the `Feed` component

#### Components To Connect

The rest of this project is there for reference and context. Limit your work to connecting the following components only:

* `/components/modules/Feed`
* `/components/modules/FeedCard`
* `/components/modules/feedcard/Comment`
* `/components/modules/feedcard/ContentImage`
* `/components/modules/feedcard/ContentLink`
* `/components/modules/feedcard/ContentText`
* `/components/modules/feedcard/CounterComments`
* `/components/modules/feedcard/CounterLikes`

Since the `FeedCard` component is already stubbed out into subcomponents, it should make the markup and logic much easier to deal with in the scope of this test.

#### Final Notes

If you are not yet familiar with React/Reflux, the code content of this project and the documentation available on the links to both projects above is very good.

If you have questions about the data returned and how it is supposed to feed into the above components... or just questions in general, please ask questions. If you need clarification, asking questions is a good thing.

Good luck!

##Getting Started

#### Requirements

1. Node
2. Gulp

Make sure you have both of these installed globally (`-g`) before proceeding below.

#### Getting up and running

1. Clone this repo from `https://github.com/PLAYERPRO/playerpro-react-test.git`
2. Run `sudo npm install` from the root directory
3. Run `gulp dev` (may require installing Gulp globally `npm install gulp -g`)
4. Your browser will automatically be opened and directed to the browser-sync proxy address and port [`http://localhost:3000/`](http://localhost:3000/)
5. You should now also have access to the mock API at [`http://localhost:4000/`](http://localhost:4000/)

Now that `gulp dev` is running, the server is up as well and serving files from the `/build` directory. Any changes in the `/app` directory will be automatically processed by Gulp and the changes will be injected to any open browsers pointed at the proxy address. This should help you move quickly while connecting components because any changes you make will automatically refresh for you.

## Project Introduction

#### ReactJS

The ReactJS files are all located within `/app/js`, structured in the following manner:

```
/components
  - Footer.js (Simple, static footer component rendered on all pages.)
  - Header.js (Simple, static header component rendered on all pages.)
/mixins
/pages
  - HomePage.js (Example home page, serving as the default route.)
  - NotFoundPage.js (Displayed any time the user requests a non-existent route.)
  - SearchPage.js (Example search page to demonstrate navigation and individual pages.)
/utils
  - APIUtils.js (General wrappers for API interaction via Superagent.)
  - AuthAPI.js (Example functions for user authorization via a remote API.)
App.js (The main container component, rendered to the DOM and then responsible for rendering all pages.)
index.js (The main javascript file watched by Browserify, responsible for requiring the app and running the router.)
Routes.js (Defines the routing structure, along with each individual route path and handler.)
```

Each module you add to your project should be placed in the appropriate directory, and required in the necessary files. Once required, they will be automatically detected and compiled by Browserify.

#### RefluxJS

The RefluxJS files are also all locationed within `/app/js`, structured in the following manner:

```
/actions
  - CurrentUserActions.js (Possible actions relevant to the current user. i.e. `checkAuth`, `login`, and `logout`.)
/stores
  - CurrentUserStore.js (Responsible for storing the current user data, while listening to any `CurrentUserActions`.)
```

Each action or store you add to your project should be placed in the appropriate directory, and required in the necessary files. The necessary logic to trigger actions and listen to stores should also be added.

#### API

The data returned at the mock API endpoint in this project is from our RESTful API. You can use GET to access resources from the API that will be used while building your components. For the scope of this test you will be using the [`GET /posts`](http://localhost:4000/posts) endpoint.
